//picom test shader
#version 330

in vec2 texcoord;

uniform sampler2D tex;
uniform float time;

vec4 default_post_processing(vec4 c);

vec4 window_shader(){
	vec4 c = texelFetch(tex, ivec2(texcoord), 0);
	c.r = c.r + (cos(time / 750 - texcoord.y / 8) + 1) / 50;
	return default_post_processing(c);
}
