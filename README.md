this is a directory of configuration files i use on my machine

heres a list of what all the configs do

alacritty:
changed the style

awesome:
changed the style and removed some unecessary things like the titlebars of windows and general clutter

newsboat:
its mostly just luke smith's config but i adjusted the colors

picom:
added opacity effects, dimming of windows, transparent clipping and adjusted the exclude rules, custom shader for alacritty that adds a CRT effect

ranger:
changed the default appllications for file extensions

rofi:
changed the style
